﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Type tipo;      //Declara variable tipo TYPE
            double numero;

            tipo = typeof(string);
            Console.WriteLine("El nombre corto es " + tipo.Name);
            Console.WriteLine("El nombre largo es " + tipo.FullName);
            Console.WriteLine("Hola Mundo");


            numero = 32.50;
            Console.WriteLine("El numero es " + numero);
            Console.WriteLine("El numero convertido es " + (int) numero);



            Console.ReadKey();

           }
    }
}
