﻿namespace _02_WindowsFormsIterativas
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxResultado = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button12 = new System.Windows.Forms.Button();
            this.ImprimirTabla = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxResultado
            // 
            this.textBoxResultado.Location = new System.Drawing.Point(348, 238);
            this.textBoxResultado.Multiline = true;
            this.textBoxResultado.Name = "textBoxResultado";
            this.textBoxResultado.Size = new System.Drawing.Size(158, 39);
            this.textBoxResultado.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(386, 166);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "label1";
            this.label1.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(523, 238);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(98, 39);
            this.button12.TabIndex = 0;
            this.button12.Text = "ImprimirNumeros";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button1_Click);
            // 
            // ImprimirTabla
            // 
            this.ImprimirTabla.Location = new System.Drawing.Point(523, 283);
            this.ImprimirTabla.Name = "ImprimirTabla";
            this.ImprimirTabla.Size = new System.Drawing.Size(98, 39);
            this.ImprimirTabla.TabIndex = 0;
            this.ImprimirTabla.Text = "ImprimirTabla\r\n";
            this.ImprimirTabla.UseVisualStyleBackColor = true;
            this.ImprimirTabla.Click += new System.EventHandler(this.ImprimirTabla_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(989, 567);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxResultado);
            this.Controls.Add(this.ImprimirTabla);
            this.Controls.Add(this.button12);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBoxResultado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button ImprimirTabla;
    }
}

