﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _02_WindowsFormsIterativas
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int numero;
            numero = Int32.Parse(textBoxResultado.Text);
            for(int cnt=0; cnt<=numero; cnt++) {
                if (cnt != numero)
                    label1.Text += cnt + " - ";
                else
                label1.Text += cnt;

            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void ImprimirTabla_Click(object sender, EventArgs e)
        {
            int[] tabla = { 11, 22, 33, 44, 55 };

            //for tradicional
            for(int cnt=0; cnt<tabla.Length; cnt++) {
                label1.Text += tabla[cnt] + " ";

            }
            //for solo colecciones
            foreach(int numero in tabla) {      //foreach recorre desde el 0 hasta arriba
                label1.Text += numero + " ";
            }
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }
    }
}
