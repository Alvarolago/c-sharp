﻿namespace _03_WindowsFormsApplicationMetodos
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.buttonSumar1 = new System.Windows.Forms.Button();
            this.labelresultado = new System.Windows.Forms.Label();
            this.buttonSunar2 = new System.Windows.Forms.Button();
            this.buttonSumar3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(306, 100);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(95, 20);
            this.textBox1.TabIndex = 0;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(306, 126);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(95, 20);
            this.textBox2.TabIndex = 0;
            // 
            // buttonSumar1
            // 
            this.buttonSumar1.Location = new System.Drawing.Point(414, 99);
            this.buttonSumar1.Name = "buttonSumar1";
            this.buttonSumar1.Size = new System.Drawing.Size(69, 46);
            this.buttonSumar1.TabIndex = 1;
            this.buttonSumar1.Text = "Sumar V1";
            this.buttonSumar1.UseVisualStyleBackColor = true;
            this.buttonSumar1.Click += new System.EventHandler(this.buttonSumar1_Click);
            // 
            // labelresultado
            // 
            this.labelresultado.AutoSize = true;
            this.labelresultado.Location = new System.Drawing.Point(315, 162);
            this.labelresultado.Name = "labelresultado";
            this.labelresultado.Size = new System.Drawing.Size(35, 13);
            this.labelresultado.TabIndex = 2;
            this.labelresultado.Text = "label1";
            // 
            // buttonSunar2
            // 
            this.buttonSunar2.Location = new System.Drawing.Point(414, 162);
            this.buttonSunar2.Name = "buttonSunar2";
            this.buttonSunar2.Size = new System.Drawing.Size(69, 46);
            this.buttonSunar2.TabIndex = 1;
            this.buttonSunar2.Text = "Sumar V2";
            this.buttonSunar2.UseVisualStyleBackColor = true;
            this.buttonSunar2.Click += new System.EventHandler(this.buttonSunar2_Click);
            // 
            // buttonSumar3
            // 
            this.buttonSumar3.Location = new System.Drawing.Point(413, 223);
            this.buttonSumar3.Name = "buttonSumar3";
            this.buttonSumar3.Size = new System.Drawing.Size(69, 48);
            this.buttonSumar3.TabIndex = 3;
            this.buttonSumar3.Text = "Sumar V3";
            this.buttonSumar3.UseVisualStyleBackColor = true;
            this.buttonSumar3.Click += new System.EventHandler(this.buttonSumar3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(910, 509);
            this.Controls.Add(this.buttonSumar3);
            this.Controls.Add(this.labelresultado);
            this.Controls.Add(this.buttonSunar2);
            this.Controls.Add(this.buttonSumar1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button buttonSumar1;
        private System.Windows.Forms.Label labelresultado;
        private System.Windows.Forms.Button buttonSunar2;
        private System.Windows.Forms.Button buttonSumar3;
    }
}

