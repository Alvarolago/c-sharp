﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TareasGeneral
{
    class Program
    {
        static void Main(string[] args)
        {
            //Creamos cadena
            String cadena;

            //Rellenamos cadena
            cadena = "HelloWorld";

            //Escribir por pantalla
            Console.WriteLine(cadena);

            //Invertir una cadena
            String invertir = "";
            for (int i = 0; i < cadena.Length; i++)
            {
                invertir += (cadena[cadena.Length - i - 1]);
            }
            System.Console.WriteLine(invertir);

            //Cadena a mayusculas
            Console.WriteLine(cadena.ToUpper());

            for(int cnt = 0; cnt<cadena.Length; cnt++) {
                if((cadena[cnt] >= 'a') && (cadena[cnt] <= 'z')) 
                {
                    cadena += (char)(cadena[cnt] - 32);
                }else {
                    cadena += cadena[cnt];
                }
            }

            //Rotar Caracteres
            String temporal = cadena.ToString();
            cadena = cadena.Remove(0, 1);
            cadena += temporal;

            System.Console.WriteLine(cadena);


            //Cadena a minusculas
            Console.WriteLine(cadena.ToLower());


            Console.WriteLine("Press any key to exit."); 
            Console.ReadKey();
        }
        


    }

}
