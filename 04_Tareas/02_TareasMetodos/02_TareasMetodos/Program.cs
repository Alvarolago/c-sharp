﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_TareasMetodos
{
    class  Program
    {
        static void Main(string[] args)
        {
            double numero = 764.783;
            int digitos = 2;
            double FNumero;

            Console.WriteLine("Indique una opcion: ");
            String opcion;
            opcion = Console.ReadLine();



            switch (opcion)
            {
                case "MetodoA":
                    Console.WriteLine("MetodoA");
                    TruncarA(numero);
                    break;
                case "MetodoB":
                    Console.WriteLine("MetodoB");
                    TruncarB(numero, out digitos);
                    break;
                case "MetodoC":
                    Console.WriteLine("MetodoC");
                    TruncarC(ref numero);
                    break;
                case "MetodoD":
                    Console.WriteLine("MetodoD");
                    TruncarD(numero, digitos);
                    break;
                case "MetodoE":
                    Console.WriteLine("MetodoE");
                    TruncarE(numero, digitos, out FNumero);
                    break;
                case "MetodoF":
                    Console.WriteLine("MetodoF");
                    TruncarF(ref numero, digitos);
                    break;


            }


            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }

        //Metodo a
        static double TruncarA(double num)
        {
            return Math.Truncate(num);
        }

        //Metodo b
        static void TruncarB(double num, out int numero)
        {
            numero = (int)Math.Truncate(num);
        }

        //Metodo c
        static void TruncarC(ref double num)
        {
            num = Math.Truncate(num);
        }

        //Metodo d
        static double TruncarD(double num, int dig)
        {
            return Math.Round(num, dig);
        }

        //Metodo e
        static void TruncarE(double num, int dig, out double finalNum)
        {
            finalNum = Math.Round(num, dig);
        }

        //Metodo F
        static void TruncarF(ref double num, int dig)
        {
            num = Math.Round(num, dig);                 //ref parametro de entrada salida
        }


    }
}
