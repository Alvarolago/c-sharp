﻿namespace _05_ConsoleApplicationClases
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCirculo1 = new System.Windows.Forms.Button();
            this.buttonCirculo2 = new System.Windows.Forms.Button();
            this.buttonCirculo3 = new System.Windows.Forms.Button();
            this.textBoxRadio1 = new System.Windows.Forms.TextBox();
            this.textBoxColor1 = new System.Windows.Forms.TextBox();
            this.textBoxColor2 = new System.Windows.Forms.TextBox();
            this.textBoxRadio2 = new System.Windows.Forms.TextBox();
            this.textBoxRadio3 = new System.Windows.Forms.TextBox();
            this.textBoxColor3 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonCirculo1
            // 
            this.buttonCirculo1.Location = new System.Drawing.Point(97, 191);
            this.buttonCirculo1.Name = "buttonCirculo1";
            this.buttonCirculo1.Size = new System.Drawing.Size(153, 65);
            this.buttonCirculo1.TabIndex = 0;
            this.buttonCirculo1.Text = "button1";
            this.buttonCirculo1.UseVisualStyleBackColor = true;
            this.buttonCirculo1.Click += new System.EventHandler(this.buttonCirculo1_Click);
            // 
            // buttonCirculo2
            // 
            this.buttonCirculo2.Location = new System.Drawing.Point(341, 193);
            this.buttonCirculo2.Name = "buttonCirculo2";
            this.buttonCirculo2.Size = new System.Drawing.Size(159, 62);
            this.buttonCirculo2.TabIndex = 1;
            this.buttonCirculo2.Text = "button2";
            this.buttonCirculo2.UseVisualStyleBackColor = true;
            // 
            // buttonCirculo3
            // 
            this.buttonCirculo3.Location = new System.Drawing.Point(582, 194);
            this.buttonCirculo3.Name = "buttonCirculo3";
            this.buttonCirculo3.Size = new System.Drawing.Size(159, 62);
            this.buttonCirculo3.TabIndex = 2;
            this.buttonCirculo3.Text = "button3";
            this.buttonCirculo3.UseVisualStyleBackColor = true;
            // 
            // textBoxRadio1
            // 
            this.textBoxRadio1.Location = new System.Drawing.Point(97, 130);
            this.textBoxRadio1.Name = "textBoxRadio1";
            this.textBoxRadio1.Size = new System.Drawing.Size(153, 20);
            this.textBoxRadio1.TabIndex = 3;
            // 
            // textBoxColor1
            // 
            this.textBoxColor1.Location = new System.Drawing.Point(97, 165);
            this.textBoxColor1.Name = "textBoxColor1";
            this.textBoxColor1.Size = new System.Drawing.Size(153, 20);
            this.textBoxColor1.TabIndex = 4;
            // 
            // textBoxColor2
            // 
            this.textBoxColor2.Location = new System.Drawing.Point(341, 165);
            this.textBoxColor2.Name = "textBoxColor2";
            this.textBoxColor2.Size = new System.Drawing.Size(159, 20);
            this.textBoxColor2.TabIndex = 5;
            // 
            // textBoxRadio2
            // 
            this.textBoxRadio2.Location = new System.Drawing.Point(341, 130);
            this.textBoxRadio2.Name = "textBoxRadio2";
            this.textBoxRadio2.Size = new System.Drawing.Size(159, 20);
            this.textBoxRadio2.TabIndex = 6;
            // 
            // textBoxRadio3
            // 
            this.textBoxRadio3.Location = new System.Drawing.Point(582, 130);
            this.textBoxRadio3.Name = "textBoxRadio3";
            this.textBoxRadio3.Size = new System.Drawing.Size(159, 20);
            this.textBoxRadio3.TabIndex = 8;
            // 
            // textBoxColor3
            // 
            this.textBoxColor3.Location = new System.Drawing.Point(582, 165);
            this.textBoxColor3.Name = "textBoxColor3";
            this.textBoxColor3.Size = new System.Drawing.Size(159, 20);
            this.textBoxColor3.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(94, 305);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(911, 523);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxRadio3);
            this.Controls.Add(this.textBoxColor3);
            this.Controls.Add(this.textBoxRadio2);
            this.Controls.Add(this.textBoxColor2);
            this.Controls.Add(this.textBoxColor1);
            this.Controls.Add(this.textBoxRadio1);
            this.Controls.Add(this.buttonCirculo3);
            this.Controls.Add(this.buttonCirculo2);
            this.Controls.Add(this.buttonCirculo1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCirculo1;
        private System.Windows.Forms.Button buttonCirculo2;
        private System.Windows.Forms.Button buttonCirculo3;
        private System.Windows.Forms.TextBox textBoxRadio1;
        private System.Windows.Forms.TextBox textBoxColor1;
        private System.Windows.Forms.TextBox textBoxColor2;
        private System.Windows.Forms.TextBox textBoxRadio2;
        private System.Windows.Forms.TextBox textBoxRadio3;
        private System.Windows.Forms.TextBox textBoxColor3;
        private System.Windows.Forms.Label label1;
    }
}

