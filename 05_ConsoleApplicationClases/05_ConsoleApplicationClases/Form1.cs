﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _05_ConsoleApplicationClases
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonCirculo1_Click(object sender, EventArgs e) {
            Circulo c1 = new Circulo(Int32.Parse(textBoxRadio1.Text), textBoxColor1.Text);
            double areaCi = c1.Area();
            label1.Text = "El area es: " + areaCi;
        }
    }
}
