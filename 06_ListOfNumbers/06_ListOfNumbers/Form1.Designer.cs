﻿namespace _06_ListOfNumbers
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxIntro = new System.Windows.Forms.TextBox();
            this.labelRes10 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.labelRes15 = new System.Windows.Forms.Label();
            this.button11 = new System.Windows.Forms.Button();
            this.labelCoinciden = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxIntro
            // 
            this.textBoxIntro.Location = new System.Drawing.Point(104, 58);
            this.textBoxIntro.Multiline = true;
            this.textBoxIntro.Name = "textBoxIntro";
            this.textBoxIntro.Size = new System.Drawing.Size(205, 39);
            this.textBoxIntro.TabIndex = 0;
            // 
            // labelRes10
            // 
            this.labelRes10.AutoSize = true;
            this.labelRes10.Location = new System.Drawing.Point(101, 151);
            this.labelRes10.Name = "labelRes10";
            this.labelRes10.Size = new System.Drawing.Size(35, 13);
            this.labelRes10.TabIndex = 4;
            this.labelRes10.Text = "label1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(407, 60);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(224, 36);
            this.button1.TabIndex = 5;
            this.button1.Text = "CrearLista10";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(407, 102);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(224, 36);
            this.button2.TabIndex = 6;
            this.button2.Text = "CrearLista15";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(637, 60);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(224, 36);
            this.button3.TabIndex = 7;
            this.button3.Text = "VaciarLista10";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(637, 102);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(224, 36);
            this.button4.TabIndex = 8;
            this.button4.Text = "VaciarLista15";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(407, 175);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(224, 36);
            this.button5.TabIndex = 9;
            this.button5.Text = "Lista15OrdenInverso";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(407, 217);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(224, 36);
            this.button6.TabIndex = 10;
            this.button6.Text = "Lista10UsandoSacar";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(407, 290);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(224, 36);
            this.button7.TabIndex = 11;
            this.button7.Text = "NumerosParesLista10";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(407, 332);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(224, 36);
            this.button8.TabIndex = 12;
            this.button8.Text = "NumerosImparesLista10";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(637, 290);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(224, 36);
            this.button9.TabIndex = 13;
            this.button9.Text = "NumerosUnDigito";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(637, 332);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(224, 36);
            this.button10.TabIndex = 14;
            this.button10.Text = "NumerosDosDigitos";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // labelRes15
            // 
            this.labelRes15.AutoSize = true;
            this.labelRes15.Location = new System.Drawing.Point(101, 175);
            this.labelRes15.Name = "labelRes15";
            this.labelRes15.Size = new System.Drawing.Size(35, 13);
            this.labelRes15.TabIndex = 15;
            this.labelRes15.Text = "label1";
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(407, 402);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(224, 36);
            this.button11.TabIndex = 16;
            this.button11.Text = "NumerosEnDosListas";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // labelCoinciden
            // 
            this.labelCoinciden.AutoSize = true;
            this.labelCoinciden.Location = new System.Drawing.Point(101, 414);
            this.labelCoinciden.Name = "labelCoinciden";
            this.labelCoinciden.Size = new System.Drawing.Size(35, 13);
            this.labelCoinciden.TabIndex = 18;
            this.labelCoinciden.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1045, 582);
            this.Controls.Add(this.labelCoinciden);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.labelRes15);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.labelRes10);
            this.Controls.Add(this.textBoxIntro);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxIntro;
        private System.Windows.Forms.Label labelRes10;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label labelRes15;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Label labelCoinciden;
    }
}

