﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _06_ListOfNumbers
{
    public partial class Form1 : Form
    {

        ListaNumeros Ln = new ListaNumeros();

        private ListaNumeros lista10, lista15;

        

        public Form1()
        {
            InitializeComponent();
            lista10 = new ListaNumeros();
            lista15 = new ListaNumeros();

        }

        private void Mostrar(ListaNumeros lista, Label label)
        {
            label.Text = "Tabla vale: ";
            for (int cnt = 0; cnt < lista.getNumeroElementos(); cnt++)
                label.Text += lista.Elemento(cnt) + " ";
        }



        private void button1_Click_1(object sender, EventArgs e)
        {
            Random r = new Random();
            int aleatorio;
            Ln.Vaciar();
            for (int cnt = 0; cnt < 10; cnt++)
            {
                aleatorio = r.Next(0, 100);
                Ln.Insertar(aleatorio);    
            }
            Mostrar(Ln, labelRes10);    //mostramos en el label
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Random r = new Random();
            int aleatorio;
            Ln.Vaciar();
            for (int cnt = 0; cnt < 15; cnt++)
            {
                aleatorio = r.Next(0, 100);
                Ln.Insertar(aleatorio);
            }
            Mostrar(Ln, labelRes15);    //mostramos en el label
        }

        //Vaciar 10 y 15
        private void button3_Click(object sender, EventArgs e)
        {
            Ln.Vaciar();
            labelRes10.Text = "";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Ln.Vaciar();
            labelRes15.Text = "";

        }

        //Orden inverso
        private void button5_Click(object sender, EventArgs e)
        {
            labelRes15.Text = "";             //Vaciamos el label
            labelRes15.Text = "Tabla vale: ";
            for (int cnt = Ln.getNumeroElementos() - 1; cnt > 0; cnt--)
            {
                labelRes15.Text += Ln.Elemento(cnt) + " ";
            }
        }

        //Mostrar 10 num utilizando sacar

        private void button6_Click(object sender, EventArgs e)
        {
            labelRes10.Text = "";
            for (int cnt=0; cnt<10; cnt++)
            {
                labelRes10.Text += Ln.sacar();
            }

        }

        //Numeros Pares e Impares
        private void button7_Click(object sender, EventArgs e)
        {
            labelRes10.Text = "";
            for (int cnt = 0; cnt < 10; cnt++)
            {
                if(Ln.Elemento(cnt) % 2 == 0)
                {
                    labelRes10.Text += Ln.Elemento(cnt);
                }
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            labelRes10.Text = "";
            for (int cnt = 0; cnt < 10; cnt++)
            {
                if (Ln.Elemento(cnt) % 2 != 0)
                {
                    labelRes10.Text += Ln.Elemento(cnt);
                }
            }

        }

        //Numeros de 2 digitos y numeros de 1 digito
        private void button9_Click(object sender, EventArgs e)
        {
            labelRes15.Text = "";
            for (int cnt = 0; cnt < 15; cnt++)
            {
                if (Ln.Elemento(cnt) / 10 > 1)
                {
                    labelRes15.Text += Ln.Elemento(cnt);
                }
            }


        }

        private void button10_Click(object sender, EventArgs e)
        {
            labelRes15.Text = "";
            for (int cnt = 0; cnt < 15; cnt++)
            {
                if (Ln.Elemento(cnt) / 10 < 1)
                {
                    labelRes15.Text += Ln.Elemento(cnt);
                }
            }

        }


        //Numeros que coinciden
        private void button11_Click(object sender, EventArgs e)
        {
            //Recorre la primera lista
            for(int cnt = 0; cnt < lista10.getNumeroElementos(); cnt++) {
                  if(lista15.Posicion(lista10.Elemento(cnt)) != -1) {
                        labelCoinciden.Text += lista10.Elemento(cnt) + " ";
                  }
            }
            //Recorre la segunda lista
            for (int cnt = 0; cnt < lista15.getNumeroElementos(); cnt++)
            {
                if (lista15.Posicion(lista15.Elemento(cnt)) != -1)
                {
                    labelCoinciden.Text += lista15.Elemento(cnt) + " ";
                }
            }

        }


    }
}
