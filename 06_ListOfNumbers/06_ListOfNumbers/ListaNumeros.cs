﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_ListOfNumbers
{
    class ListaNumeros
    {

        int num;                            //Inicializacion de los datos solo en constructores
        int[] elementos;




        //Constructor
        public ListaNumeros() {
            num = 0;
            elementos = new int[100];
        }



        public ListaNumeros(int numero, int valor)
        {
            num = 0;
            elementos = new int[100];
            //Inicializamos
            for (int cnt = 0; cnt < numero; cnt++)
                elementos[cnt] = valor;
            num = numero;
        }


        public int getNumeroElementos()
        {
            return num;
        }


        public void Insertar(int valor)
        {
            if(num < 100)
            {
                elementos[num] = valor;
                num++;
                Console.WriteLine(num);
            }
 
        }


        public int Primero()
        {
            return elementos[0];
        }

        public int Ultimo()
        {
            return elementos[num-1];
        }


        public int sacar()
        {
            int valor = Primero();
            
            for (int cnt = 0; cnt < num; cnt++)
            {
                elementos[cnt] = elementos[cnt + 1];
            }
            num--;
            
            return valor;
            
        }


        public int Elemento(int posicion)
        {
            if ((num > 0) && ((posicion >= 0) && (posicion < num)))       //Si el elemento no existe
            {
                return elementos[posicion];
            }
            else
                return -10000;
            
        }

        public int Posicion(int valor)
        {
            int contador = 0;
            for (int cnt = 0; cnt < elementos.Length; cnt++)
            {
                if (valor == elementos[cnt])
                {
                    return contador;
                }
                else { contador++; }
            }
            return -1;
        }

        public bool Vacia()
        {
            if (num == 0)
                return true;
            else
                return false;
        }


        public void Vaciar()
        {
            for (int cnt = 0; cnt < elementos.Length; cnt++)     //Eliminamos todos los elementos de la lista de numeros
                elementos[cnt] = 0;

            num = 0;                            //Inicializamos el numero de elementos
        }


     
    }
}
