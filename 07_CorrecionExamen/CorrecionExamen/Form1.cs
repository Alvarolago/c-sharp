﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CorrecionExamen
{
    public partial class Form1 : Form
    {
        Fraccion f1, f2, f3, f4, f5;
        public Form1()
        {
            InitializeComponent();
            f1 = new Fraccion(16, 3);
            f2 = new Fraccion(10, 10);
            f3 = new Fraccion(6, 5);
            f4 = new Fraccion(4, 8);
            f5 = new Fraccion(13, 7);
        }

        private void button1_Click(object sender, EventArgs e)
        {

            f1.Multiplicar(f2);
            labelRes.Text = "f1 " + f1.Cadena() + "\n";

        }

        private void button2_Click(object sender, EventArgs e)
        {
            f4.Simplificar();
            labelRes.Text = "f1 simplificado" + f4.Cadena();

        }
    }
}

