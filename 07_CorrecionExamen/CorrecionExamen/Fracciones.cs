﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorrecionExamen
{
    class Fraccion
    {
        private int numerador, denominador;

        public Fraccion(int n)
        {
            numerador = n;
            denominador = 1;
        }
        public Fraccion(int n, int d)
        {
            numerador = n;
            denominador = d;
        }

        //SI TIENE AMBOS
        public void Multiplicar(Fraccion f)
        {
            numerador = numerador * f.numerador;
            denominador = denominador * f.denominador;
        }

        public void Dividir(Fraccion f)
        {
            numerador = numerador / f.numerador;
            denominador = denominador / f.denominador;
        }


        public void Simplificar()
        {

            for (int cnt = 2; cnt <= 13; cnt++)
            {
                if ((numerador % cnt == 0) && (denominador % cnt == 0))
                {
                    numerador = numerador / cnt;
                    denominador = denominador / cnt;
                }
            }

        }



        public string Cadena()
        {
            return numerador + " / " + denominador;
        }



    }
}
