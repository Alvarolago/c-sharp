﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RestauranteExer
{
    public partial class Form1 : Form
    {
        private Restaurante miRest;
        private Mesa[] misMesas = new Mesa[4];
        public Form1()
        {
            InitializeComponent();
            miRest = new Restaurante();
            //Declaramos las mesas según las queramos llenar
            misMesas[0] = new Mesa(5, 3, EstadoMesa.Ocupada, "");
            misMesas[1] = new Mesa(5, 5, EstadoMesa.Ocupada, "");

        }


        private void button1_Click(object sender, EventArgs e)
        {
            //Getter de Mesas
            miRest.setMesas(misMesas[0],0);
            miRest.setMesas(misMesas[1],1);

            miRest.porCobrar();
            label1.Text = miRest.getporCobrar().ToString();
        }
    }
}
