﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestauranteExer
{
    public enum EstadoMesa { Libre, Reservada, Ocupada };
    class Mesa
    {
        private int nOcupantesMax, nOcupanteAct;
        private EstadoMesa estado;

        private string horaOcupacion;


         //Constructor
        public Mesa()
        {
            nOcupantesMax = 4;
            nOcupanteAct = 0;
            estado = EstadoMesa.Libre;
            horaOcupacion = "";
        }
        //Getter && Setter
        public int getnOcupantesMax() { return nOcupantesMax; }
        public int getnOcupantesAct() { return nOcupanteAct; }
        public EstadoMesa getEstadoMesa() { return estado; }
        public string getHoraOcupacion() { return horaOcupacion; }



        public Mesa(int nOcMax, int nOcAct, EstadoMesa e, string horaR) 
        {
            nOcupantesMax = nOcMax;
            nOcupanteAct = nOcAct;
            estado = e;
            horaOcupacion = horaR;
        }

        //Ocupar
        public void Ocupar(int nOcupantes, string hora) 
        {
            if((estado == EstadoMesa.Libre) && (nOcupantes <= nOcupantesMax)) 
            {
                estado = EstadoMesa.Ocupada;
                nOcupanteAct = nOcupantes;
                horaOcupacion = hora;
            }

        }

        public int Cobrar() 
        {
            int cobrado = 0;
            if(estado == EstadoMesa.Ocupada) {
                estado = EstadoMesa.Libre;
                horaOcupacion = "";
                cobrado = nOcupanteAct * 10;
            }
            return cobrado;
        }

    }
}
