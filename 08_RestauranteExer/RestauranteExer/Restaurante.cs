﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestauranteExer
{

    class Restaurante
    {
        //Atributos
        private Mesa[] misMesas = new Mesa[4];
       
        private float cajaResultante;
        private int pLibres, pOcupadas, cPorCobrar;
        private int precioComensal = 10;

        //Getter && Setter
        public void setCajaActual(float caj){ cajaResultante = caj; }

        

        public Restaurante()
        {
            misMesas[0] = new Mesa(5, 0, EstadoMesa.Libre, "");
            misMesas[1] = new Mesa(5, 0, EstadoMesa.Libre, "");
            misMesas[2] = new Mesa(5, 2, EstadoMesa.Libre, "");
            misMesas[3] = new Mesa(5, 0, EstadoMesa.Libre, "");
        }

        public Mesa getMesas(int i) { return misMesas[i]; }
        public void setMesas(Mesa m1, int i) { misMesas[i] = m1; }





        public void plazasLibres(Mesa m)
        {
            if (m.getEstadoMesa() == EstadoMesa.Libre)
                pLibres += m.getnOcupantesMax();

        }

        public void plazasOcupadas(Mesa m)
        {
            if (m.getEstadoMesa() == EstadoMesa.Ocupada)
                pOcupadas += m.getnOcupantesAct();
               
        }


        public void porCobrar()
        {
            for (int cnt = 0; cnt < 3; cnt++)
            {
                if (misMesas[cnt].getEstadoMesa() == EstadoMesa.Ocupada)
                {
                    cPorCobrar += (misMesas[cnt].getnOcupantesAct() * precioComensal);
                }
            }

        }




        public int getporCobrar() { return cPorCobrar; }


    }
    

}
