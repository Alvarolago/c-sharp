﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_EjercicioProgramaTV
{
    public enum EspacioHora { PrimeraHora, Matinal, Mediodia, Tarde, Noche, Ninguno }
    public enum Contenido { Informativo, Entretenimiento, Consurso, Pelicula, Serie, Ninguno}
    public enum Dia { Lunes, Martes, Miercoles, Jueves, Viernes, Sabado, Domingo}


    class Programas
    {

        private string nombre;
        private int duracion;
        private EspacioHora espacioh;
        private Contenido cont;
        private Dia day;
        public Programas()
        {
            nombre = "";
            duracion = 0;
        }

        public Programas(string nom, int dur, EspacioHora eh, Contenido c, Dia d)
        {
            nombre = nom;
            duracion = dur;
            espacioh = eh;
            cont = c;
            day = d;
        }


        public string getNombre() { return nombre; }
        public Dia getDay() { return day; }
        public EspacioHora getEspacioHora() { return espacioh; }

        public void setMinutos(int newMin) { duracion = newMin; }




    }
}
