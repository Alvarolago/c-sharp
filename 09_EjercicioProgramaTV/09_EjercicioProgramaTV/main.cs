﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_EjercicioProgramaTV
{
    class main
    {
        private int contador;
        private Programas[] misProgramas = new Programas[5];

        public void asignarNuevo()
        {
            misProgramas[0] = new Programas("Elchiringuito", 160, EspacioHora.Noche, Contenido.Entretenimiento, Dia.Lunes);
            contador++;
        }

        public void eliminarPrograma(Dia dia, EspacioHora eh)
        {
            for (int cnt = 0; cnt < misProgramas.Length; cnt++)
            {
                {
                    if ((misProgramas[cnt].getEspacioHora() == eh) && (misProgramas[cnt].getDay() == dia))
                    {
                        misProgramas[cnt] = new Programas();
                    }
                }
            }

        }


        public void modificarMinutos(Dia dia, EspacioHora eh, int newMinuts)
        {
            for (int cnt = 0; cnt < misProgramas.Length; cnt++)
            {
                if ((misProgramas[cnt].getEspacioHora() == eh) && (misProgramas[cnt].getDay() == dia))
                {
                    misProgramas[cnt].setMinutos(newMinuts);
                }
            }
        }


        public void mostrarProgramacion()
        {
            for (int cnt = 0; cnt < misProgramas.Length; cnt++)
            {
                Console.WriteLine(misProgramas[cnt]);
            }
        }




    }
}
