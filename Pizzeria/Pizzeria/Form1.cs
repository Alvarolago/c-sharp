﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pizzeria
{
    public partial class Form1 : Form
    {
        int[] tamaños = new int[4];
        int[] masas = new int[4];
        int[] extras = new int[5];

        public int veces=0;
        public int total;
        int cnt = 0;
       
        public Form1()
        {
            InitializeComponent();



            //Precios según el tamaño
            tamaños[0] = 10;
            tamaños[1] = 9;
            tamaños[2] = 8;
            tamaños[3] = 6;

            //Precios según la masa
            masas[0] = 3;
            masas[1] = 4;
            masas[2] = 5;
            masas[3] = 5;

            //Precios según los extras
            extras[0] = 0;
            extras[1] = 1;
            extras[2] = 2;
            extras[3] = 3;
            extras[4] = 4;
        }

        private void uncheckAll()
        {
            foreach (int checkedItemIndex in checkedListBox1.CheckedIndices) { checkedListBox1.SetItemChecked(checkedItemIndex, false); }
        }

        private void uncheckTamaño()
        {
            foreach (int checkedItemIndex in checkedListBoxTamaño.CheckedIndices) { checkedListBoxTamaño.SetItemChecked(checkedItemIndex, false); }
        }

        private void unckeckMasa()
        {
            foreach(int checkedItemIndex in checkedListBoxMasa.CheckedIndices) { checkedListBoxMasa.SetItemChecked(checkedItemIndex, false); }
        }


        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkedListBox1.Enabled = true;
            checkedListBoxMasa.Enabled = true;
            checkedListBoxTamaño.Enabled = true;

            //ACTIVAMOS EL BOTON CALCULAR IMPORTE
            button3.Enabled = false;
            //DESMARCAMOS TODOS
            uncheckAll();

            if (comboBox1.SelectedIndex == 0)
            {
                uncheckAll();
                uncheckTamaño();
                unckeckMasa();
                checkedListBox1.SetItemChecked(0, true);
                checkedListBox1.SetItemChecked(1, true);
            }

            if (comboBox1.SelectedIndex == 1){

                uncheckAll();
                uncheckTamaño();
                unckeckMasa();
                checkedListBox1.SetItemChecked(2, true);
                checkedListBox1.SetItemChecked(3, true);
                
                checkedListBoxTamaño.SetItemChecked(3, true);
                checkedListBoxTamaño.Enabled = false;
 
            }


            if (comboBox1.SelectedIndex == 2)
            {
                uncheckAll();
                uncheckTamaño();
                unckeckMasa();
                checkedListBox1.SetItemChecked(4, true);
                checkedListBox1.SetItemChecked(5, true);
            }

            if (comboBox1.SelectedIndex == 3)
            {
                uncheckAll();
                uncheckTamaño();
                unckeckMasa();
                checkedListBox1.SetItemChecked(6, true);
                checkedListBox1.SetItemChecked(7, true);
                checkedListBox1.SetItemChecked(8, true);
                checkedListBox1.SetItemChecked(9, true);
                checkedListBoxMasa.SetItemChecked(0, true);
                checkedListBoxMasa.Enabled = false;
                checkedListBox1.Enabled = false;
            }

            checkedListBox1.GetItemChecked(1);
            int ingredientes = comboBox1.SelectedIndex;


        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            button3.Enabled = true;

             

            //LIMPIAMOS EL LISTVIEW
            listView1.Items.Clear();

            //INICIALIZAMOS LOS GRUPOS
            ListViewGroup tipoPizza = new ListViewGroup("TipoPizza", HorizontalAlignment.Left);
            ListViewGroup ingredientesPizza = new ListViewGroup("Ingredientes", HorizontalAlignment.Left);
            ListViewGroup tamañoPizza = new ListViewGroup("TamañoPizza", HorizontalAlignment.Left);
            ListViewGroup masaPizza = new ListViewGroup("MasaPizza", HorizontalAlignment.Left);

            //LIMPIAR LOS MARCADOS
            for (int cnt = 0; cnt <= checkedListBox1.CheckedItems.Count - 1; cnt++)
            {
                listView1.Items.Add(new ListViewItem(checkedListBox1.CheckedItems[cnt] + "", ingredientesPizza));
            }

            MessageBoxButtons buttons = MessageBoxButtons.OK;

            //SI ESTÁ VACÍA LA CASILLA
            if (checkedListBoxMasa.CheckedItems.Count == 0 || checkedListBoxTamaño.CheckedItems.Count == 0 || checkedListBoxMasa.CheckedItems.Count == 0)
            {
                DialogResult = MessageBox.Show("Debe Seleccionar todos los campos", "Seleccione", buttons, MessageBoxIcon.Error);
                listView1.Items.Clear();
                button3.Enabled = false;
            }
            else
            {
                listView1.Items.Add(new ListViewItem(comboBox1.SelectedItem + "", tipoPizza));
                listView1.Items.Add(new ListViewItem(checkedListBoxMasa.CheckedItems[0] + "", masaPizza));
                listView1.Items.Add(new ListViewItem(checkedListBoxTamaño.CheckedItems[0] + "", tamañoPizza));

            }

            //Mínimo de extras
            if(checkedListBox1.CheckedItems.Count == 3)
            {
                MessageBoxButtons buttons1 = MessageBoxButtons.OK;
                DialogResult = MessageBox.Show("Debe elegir como mínimo 2 extras", "Extras", buttons1, MessageBoxIcon.Warning);
                listView1.Items.Clear();
            }
            

            //GRUPOS
            listView1.Groups.Add(tipoPizza);
            listView1.Groups.Add(ingredientesPizza);
            listView1.Groups.Add(tamañoPizza);
            listView1.Groups.Add(masaPizza);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            button3.Enabled = false;
            listView1.Items.Clear();
        }






        private void checkedListBoxTamaño_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selNdx = checkedListBoxTamaño.SelectedIndex;

            foreach (int cbNdx in checkedListBoxTamaño.CheckedIndices)
                if (cbNdx != selNdx) checkedListBoxTamaño.SetItemChecked(cbNdx, false);
            

        }

        private void button3_Click(object sender, EventArgs e)
        {

            totalFunction();


           MessageBox.Show("Factura", "El importe de su pedido es: " + total, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void Salir_Click(object sender, EventArgs e)
        {
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult = MessageBox.Show("Desea salir?", "Salir", buttons, MessageBoxIcon.Warning);
            if (DialogResult == DialogResult.Yes) {
                this.Close();
            }

        }


        private void checkedListBoxMasa_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selNdx = checkedListBoxMasa.SelectedIndex;

            foreach (int cbNdx in checkedListBoxMasa.CheckedIndices)
                if (cbNdx != selNdx)  checkedListBoxMasa.SetItemChecked(cbNdx, false);
                
  
        }


        private void alwaysChecked()
        {
            if (comboBox1.SelectedIndex == 0)
            {
                checkedListBox1.SetItemChecked(0, true);
                checkedListBox1.SetItemChecked(1, true);
                maxIngredients(6);
 
            }

            if (comboBox1.SelectedIndex == 1)
            {
                checkedListBox1.SetItemChecked(2, true);
                checkedListBox1.SetItemChecked(3, true);
                maxIngredients(6);

            }
            if(comboBox1.SelectedIndex == 2)
            {
                checkedListBox1.SetItemChecked(4, true);
                checkedListBox1.SetItemChecked(5, true);
                maxIngredients(6);
  
            }

        }


        void maxIngredients(int ing)
        {
            if(checkedListBox1.CheckedItems.Count > ing)
            {
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult = MessageBox.Show("Máximo 4 ingredientes extra", "Maximo Ingredientes", buttons, MessageBoxIcon.Warning);

                //DESMARCAMOS TODOS
                uncheckAll();
                alwaysChecked();
            }
           
        }

        void cantVegetariana(int num)
        {
            if (comboBox1.SelectedIndex == 1 && checkedListBox1.GetItemChecked(num))
            {
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult = MessageBox.Show("En la Vegetariana no se puede incluir productos de origen animal", "Vegetariana", buttons, MessageBoxIcon.Warning);
                checkedListBox1.SetItemChecked(num, false);
            }
        }


        //One click function
        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            alwaysChecked();

            //Excepción vegetariana
            cantVegetariana(0);
            cantVegetariana(5);
            cantVegetariana(11);


        }
        //Double click function
        private void checkedListBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            alwaysChecked();
        }


        private void totalFunction()
        {
            total = 0;

            if(checkedListBoxTamaño.CheckedIndices.Count == 0 || checkedListBoxMasa.CheckedIndices.Count == 0) {
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult = MessageBox.Show("Debe elegir al menos algo en cada categoría", "Seleccionar", buttons, MessageBoxIcon.Warning);
            }
            else {
                if (comboBox1.SelectedIndex == 1 && checkedListBoxTamaño.SelectedIndex == -1)
                        checkedListBoxTamaño.SelectedIndex = 3;

                total += tamaños[checkedListBoxTamaño.SelectedIndex];
                total += masas[checkedListBoxMasa.SelectedIndex];
                total += extras[checkedListBox1.CheckedItems.Count - 2];
            }


        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
